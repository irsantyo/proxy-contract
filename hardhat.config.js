require('@typechain/hardhat');
require('@nomiclabs/hardhat-waffle');
require('@nomiclabs/hardhat-etherscan');
require('@nomiclabs/hardhat-ethers');
require('hardhat-gas-reporter');
require('dotenv/config');
require('solidity-coverage');
require('hardhat-deploy');
require('solidity-coverage');

const config = {
  defaultNetwork: 'hardhat',
  networks: {
    hardhat: {
      chainId: 1337,
    },
    localhost: {
      chainId: 1337,
    },
    rinkeby: {
      chainId: 4,
      url: 'https://rinkeby.infura.io/v3/b2d272227f034563965236224da506ed',
      accounts: process.env.PK ? [process.env.PK] : [],
    },
  },
  solidity: '0.7.6',
  gasReporter: {
    enabled: true,
    currency: 'USD',
    outputFile: 'gas-report.txt',
    noColors: true,
  },
  namedAccounts: {
    deployer: {
      default: 0, // here this will by default take the first account as deployer
    },
  },
  mocha: {
    timeout: 200000, // 200 seconds max for running tests
  },
  etherscan: {
    apiKey: process.env.ETHERSCAN_KEY,
  },
};

module.exports = config;
